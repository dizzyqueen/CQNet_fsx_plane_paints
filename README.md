## Welcome to the ComputerQueen.Net GitLab Repository of Flight Simulator X Aircraft Repaints
All the Aircraft Texture Repaints are in the ComputerQueen.Net Livery V2.0 created by Kevin Harrington.  These are for aircraft installed into Microsoft Flight Simulator X: Steam Edition (FSX-SE), and encompass repaints for both Default and Add-On Aircraft.  Credit will be given to the developer of the aircraft model, whenever possible, inside the specific model texture folder's README.md file.

## Examples
### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_A320SL">CQ.Net A320neo (CFM Sharklets) (Aerosoft)</a>
<IMG src="N320CQ.png" style="width:512px;height:277px;">
  
### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_A321">CQ.Net A321 (Aerosoft)</a>
<IMG src="N321CQ-005.png" style="width:512px;height:277px;">
  
### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_B738">CQ.Net B737-800 (FSX Default)</a>
<IMG src="N738CQ-003.png" style="width:512px;height:277px;">
  
### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_B737-MAX8">CQ.Net B737-MAX 8 (TDS)</a>
<IMG src="N738CM-008.PNG" style="width:512px;height:277px;">

### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_MD-90">CQ.Net MD-90 (SGA)</a>
<img src="N790CQ-001.png" style="width:512px;height:277px;">

### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_MD-83">CQ.Net MD-83 (FSND)</a>
<IMG SRC="N717CQ-002.png" style="width:512px;height:277px;">

### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_B744LCF">CQ.Net Boeing 747-400 LCF (POSKY)</A>
<IMG SRC="N744BQ-005.png" style="width:512px;height:277px;">

### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_B777-9X">CQ.Net Boeing 777-9X (SKYSPIRIT2019-777)</A>
<IMG SRC="N779CQ-001.PNG" style="width:512px;height:277px;">

### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_B737-700">CQ.Net B737-700 (Kitty Hawk)</a>
<IMG src="N737CQ-001.png" style="width:512px;height:277px;">

### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_B737-MAX7">CQ.Net B737-MAX 7 (TDS)</a>
<IMG src="N737CM-001.png" style="width:512px;height:277px;">
  
### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_B737-MAX9">CQ.Net B737-MAX 9 (TDS)</a>
<IMG src="N739CM-041.png" style="width:512px;height:277px;">
  
### <a href="https://gitlab.com/dizzyqueen/CQNet_fsx_plane_paints/tree/master/CQ_A350-900">CQ.Net Airbus A350-900XWB (FSPainter)</a>
<IMG src="N350CQ-001.png" style="width:512px;height:277px;">

### Support or Contact

Contact the repository owner, Kevin Harrington, for any support issues <a href="mailto:kevinh3@gmail.com">here</a>.
